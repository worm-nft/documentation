# WORM Multichain NFT Dropper

WORM NFT Dropper is a program that allows you to mint NFT cards/vourcher and transfer to multiple crypto addresses. It uses The ERC1155: The Multi-Token Standard

# Introduction to ERC1155
Aiming to encapsulate the best of both worlds, ERC1155 was introduced by Enjin, designed to be both fungibility-agnostic and gas-efficient. Unlike its predecessors, ERC1155 enables the management of both fungible and non-fungible tokens under a single smart contract. This not only economizes on-chain data but significantly reduces gas fees, a major “hoorah” for developers and users alike. 

ERC1155's unique architecture lends itself to a myriad of use cases, bringing a new era of digital asset management on the Ethereum blockchain.

# The Technical Anatomy of ERC1155
## Single Contract, Multiple Tokens
ERC1155 embodies a revolutionary approach by housing multiple tokens within a single contract. This is starkly different from previous standards like ERC20 and ERC721, which require separate contracts for each token type. For instance, in a gaming scenario, one could have different assets like gold, silver, and unique items like swords or shields all managed within a single ERC1155 contract. This significantly reduces the complexity and cost usually associated with deploying and managing multiple contracts.

## Batch Operations
The ingenuity of ERC1155 is further showcased in its batch operations. Functions like balanceOfBatch and safeBatchTransferFrom enable querying and transferring multiple tokens in a single transaction, respectively. This is a significant upgrade in terms of efficiency and gas savings.

## Token Minting
Token minting in ERC1155 is facilitated through the mint and _mintBatch functions, allowing for the creation of new tokens either on-demand or in batches. This flexibility is crucial for various use cases, including gaming, where different types of assets need to be minted at different times or in different quantities.



## NFT Pinning Services
- [nft.storage](https://nft.storage) 
- [Pinata](https://pinata.cloud).

## Features
1. You can compile and upload NFT smart contract to the blockchain(maninet or testnet) without using Remix editor
2. Supports both Testnet and Mainet on Ethereum, Polygon, BSC, Optimism, Arbitrum, etc network or any L2 network on ethereum
3. Mint Batch NFT (You can mint unlimited  NFTs @ very low gas fee)
4. Mint Single NFT
5. Generate Batch NFT Metadata (You can generate upto unlimited NFT metadata to be uploaded amd pinned to [nft.storage](https://nft.storage) and [Pinata](https://pinata.cloud))
6. Transfer Single NFT
7. Transfer Multiple

## STEP 1 - Setup 

To install and run Worm Multichain NFT, you must have NPM installed. Make sure nodejs is installed on your machine [https://nodejs.org/en/download.](https://nodejs.org/en/download)

1. Unzip the file into the `worm` directory/folder:

2. Install the NPM dependencies:

    ```shell
    npm install
    ```
3. After all dependencies have been installed. Add the `worm` command to your `$PATH`. This makes it easier to run `worm` command from anywhere on your computer:

    ```
    npm link
    ```

## STEP 2 -Activate License Key 
To use Worm Multichain NFT Dropper/Minter, you need a valid LIcense Key.

1. Open the confirguration in a code editor [`./config/default.js`](./config/default.js) file

2. Paste your license key and save file after making changes.
    ```shell
        licensekey:"",
    ```

3. Run command below in your terminal/cmd to activate your license
    ```shell
        worm activate-license

        =======Worm Multichain NFT Dropper/Minter============
        License key activated successfully
        >>> `worm help` to view all available commands
    ```

## STEP 3 -Configuration 
1. Open the confirguration [`./config/default.js`](./config/default.js) file:

    ```shell
    # Required ✋
    INFURA_KEY: "",
    ACCOUNT_PRIVATE_KEY : "",

    # Optional 🤘
    # Add your etherscan if you want to verify your contract
    ETHERSCAN_API_KEY :"",

    #Smart contract config
    name: "$2000 USDC Voucher",
    symbol: "$2000 USDC Voucher",
    ipfs_cid : "bafybeiglre2yfttwgzmjvtrfnweya22pjxbafc6hifmqvjn33lxbzv7lqm",
    CONTRACT_NAME : "USDCVoucher",

    # Required ✋- NFT metadata config
    # This is the matadata of the NFT you want to mint
    name: "$2000 USDC Voucher",
    external_url: "https://www.yourwebsite.com/",
    description: "Congratulations! You can exchange this NFT voucher for $2000 USDC at the official site: https://yourwebsite.com",
    image:"bafkreibr4pfguxlqjipb3oo627ya7sopev7m6iw5n33adxiryo7adhcik4",
    properties: {
        "website": "https://www.yourwebsite.com/",
        "instruction": "Connect your wallet to exchange this NFT voucher $2000 USDC"
    },

    # Required ✋- Worm NFT LIcense Key
    licensekey : "",

    # Required ✋- blockchain network to deploy NFT
    defaultNetwork: "goerli",
    ```
### Configuration Descriptions
- **INFURA_KEY** - Has your infura api key. You can create account on [https://www.infura.io/](https://www.infura.io). This facilitates 
- **ACCOUNT_PRIVATE_KEY** - Has your account private key
- **ETHERSCAN_API_KEY** - Has your etherscan key. This is optional
- **name** - Has the name of the NFT
- **symbol** - Has the symbol of the NFT
- **ipfs_cid** - Has the IPFS CID of the NFT metadata
- **CONTRACT_NAME** - A unique name that identifies your NFT smart contract on the blockchain
- **external_url** - Has the external website link that will be shown on your NFT
- **description** - Has the description that will be shown on your NFT
- **image** - Has the IPFS CID your NFT image
- **properties** - Has the basic properties of your NFT
- **licensekey** - Has the License Key of Worm Multichain NFT Minter/Dropper
- **defaultNetwork** - Has the blockchain network name to which the NFT will be deployed

### defaultNetwork (Blockchains network supported)
7(Ethereum, Polygon, Base, Optimism, Starknet, BNB & Linea) L2 Blockchains are currently supported running on the ethereum node/VM. 

You can add more L2(Layer 2) networks that runs on Ethereum Node by modifying the [`hardhat.config.js`](hardhat.config.js) file. See the [Hardhat configuration docs](https://hardhat.org/config/) to learn how to configure a JSON-RPC node. Once you've added a new network to the Hardhat config, you can use it by setting the `HARDHAT_NETWORK` environment variable to the name of the new network when you run `minty` commands. Alternatively, you can change the `defaultNetwork` in `hardhat.config.js` to always prefer the new network.

| Blockchain Nwtwork |  defaultNetwork |
| ------------- | ------------- |
| Ethereum Goerli(Testnet)  | **goerli**
| Ethereum Mainnet | **ethereum**
| Ethereum Sepolia (Testnet) | **sepolia**
| Polygon Mainnet | **polygon**
| Polygon Mumbai | **mumbai**
| Arbitrum Goerli(Testnet) |   **arbitrum_goerli**
| Arbitrum Mainnet |   **arbitrum**
| Arbitrum Sepolia (Testnet) |   **arbitrum_sepolia**
| Base Goerli(Testnet) |   **base_goerli**
| Base Mainnet |   **base**
| Base Sepolia (Testnet) |   **base_sepolia**
| Optimism Goerli(Testnet) |   **optimism_goerli**
| Optimism Mainnet |  **optimism**
| Optimism Sepolia (Testnet) |   **optimism_sepolia**
| Starknet Goerli(Testnet) |   **starknet_goerli**
| Starknet Mainnet |   **starknet**
| Starknet Sepolia (Testnet) |   **starknet_sepolia**
| BNB Smart Chain(Testnet) |   **bnbsmartchain_testnet**
| BNB Smart Chain Mainnet |   **bnbsmartchain**
| Linea Goerli(Testnet) |   **linea_goerli**
| Linea Mainnet |   **linea**


## STEP 4 - Upload NFT Image
1. To upload our files to the decentralized storage IPFS, we can make use of an easy-to-use tool NFT Storage. https://nft.storage

2. Sign in to NFT Storage and upload your image files for the NFT. You should see something like this once they've been uploaded successfully:
![image](https://github.com/kernelwares/MEVBOT-Web3/blob/main/image1.png)

3. Click on **Actions** and copy the CID of the NFT image; we will need it for the metadata of NFT Metadata collection.
![image](https://github.com/kernelwares/MEVBOT-Web3/blob/main/image2.png)

4. Open the confirguration in a code editor [`./config/default.js`](./config/default.js) file

5. Paste the CID of the NFT image you copied earlier and save file after making changes.
    ```shell
        image:"bafkreibr4pfguxlqjipb3oo627ya7sopev7m6iw5n33adxiryo7adhcik4",
    ```
Note: There is a sample NFT image you can use as a test

## STEP 5 - Generating NFT Metadata Collection

2. Run command below in your terminal/cmd
    ```shell
        worm generate-metadata 1000 

        >>> Generating NFT Metadata Collection...
        >>> 🌿Successfully generated 1000 NFT Metadata Collection
        >>> Check folder ./metadata and upload all the files to https://car.ipfs.io/
        >>> https://car.ipfs.io/ helps archive files in IPFS compatible content-addressed archive (.car) format.
    ```
- Note: This will generate 1000 NFT Metadatas. You can generate any number of metadata you want.


## STEP 6 - Uploading NFT Metadata Collection
To efficiently upload all the NFT Metadata JSON files generated in `STEP 3 ` into IPFS, we will archive them in content-addressed format. https://car.ipfs.io/ helps archive files in IPFS compatible content-addressed archive (.car) format.

1. Head over to IPFS CAR [https://car.ipfs.io/](https://car.ipfs.io/), select all the files and upload all JSON files. Once uploaded, download the .car file.

2. Now upload the .car file to [https://nft.storage](https://nft.storage). All our JSON files are now stored on IPFS in an archived manner. 

3. Copy the CID of the uploaded .car file, and you should be able to access JSON files by just entering the file name at the end of the URL, for example:
![image](https://github.com/kernelwares/MEVBOT-Web3/blob/main/image2.png)

4. Open the confirguration in a code editor [`./config/default.js`](./config/default.js) file

5. Paste the CID of the NFT metadata(.car) you copied earlier and save file after making changes.
    ```shell
        ipfs_cid : "bafybeiglre2yfttwgzmjvtrfnweya22pjxbafc6hifmqvjn33lxbzv7lqm",
    ```

## STEP 7 - Compile the smart contract
You need to compile your contract so that the Ethereum virtual machine (EVM) can understand it.
1. Run the command below in your terminal/cmd to compile the smart contract
    ```shell
    worm compile

    >>> Compiling smart contract
    >>> Compiling 16 files with 0.7.3
    >>> ...
    ```
## STEP 8 - Deploy the contract
1. Run the command below in your terminal/cmd
    ``` shell
        worm deploy

        >>> deploying contract for token $2000 USDC Voucher ($2000 USDC Voucher) to network "goerli"...
        >>> deployed contract for token $2000 USDC Voucher ($2000 USDC Voucher) to 0x23FE0281AEDD0ddC05224B0e69c28c57b26A1802(network: rinkeby)
        >>> Writing deployment info to minty-deployment.json
    ```
This deploys to the network configured in [`./config/default.js`](./config/default.js), which is set to the `goerli` network by default.

When the contract is deployed, the address and other information about the deployment is written to `minty-deployment.json`. This file must be present for subsequent commands to work.


## STEP 9 - Mint NFT Token(Single & Batch)
### Mint Single NFT
This will create a new ERC1155 NFT on-demand. After sometime of minting, you can see the NFT on Opensea.
    ``` shell
        worm mint-single --nftid 1
    ```
- Help: Run the command below to see how to use the `mint-single` command  
    ``` shell 
        worm help mint-single
    ``` 
### Mint Batch/Multiple NFTS
This will create a new ERC1155 NFT in batches. After sometime of minting, you can see the NFT on Opensea.
    ``` shell
        worm mint-batch --quantity 1000
    ```
the command will mint 1000 quantity of NFT in a batch. 
- Note: Ensure the total number of NFT you want to mint in batch is the same number of NFT Metadata you generated earlier in STEP 5. You can also mint the batch NFT to a desired/external wallet other than the signers wallet by using the **--owner** flag. E.g:

    ``` shell
        worm mint-batch --quantity 1000 --owner 0x803356dD9D95bF252dB005aeD7c03D3dC63d7d73
    ```

- Help: Run the command below to see how to use the `mint-batch` command  
    ``` shell 
        worm help mint-batch
    ``` 

## STEP 10 - Airdrop/Transfer NFT Token(Single & Batch)
After minting your NFT assets, it's now time to airdrop/transfer them to users.

### Transfer Single NFT
1. Run the command below in your terminal/cmd `worm transfer <nft-id> <to-address>`.

    ``` shell
        worm transfer 1 0x803356dD9D95bF252dB005aeD7c03D3dC63d7d73
    ```
This will transfer NFT with the ID of **1** to the address **0x803356dD9D95bF252dB005aeD7c03D3dC63d7d73**

- Help: Run the command below to see how to use the `transfer` command.

    ``` shell 
        worm help transfer
    ``` 

### Transfer Batch NFTs
1. Open the wallet file [`wallet.txt`](wallet.txt) file and paste the wallet address which you want to
transfer NFT to. Save file after making changes.

2. Run the command below in your terminal/cmd `worm transfer-batch`
    ``` shell
        worm transfer-batch
    ```
This will transfer each NFT to the wallet address in the wallet.txt file

- NOTE: MAke sure the number of wallet address corresponds with the number of NFT you minted. E.g 
If you minted 1000 NFTs, you must send the 1000 NFTs to 1000 wallet addresses.

- Help: Run the command below to see how to use the `transfer-batch` command  
    ``` shell 
        worm help transfer-batch
    ``` 

# Contact/Support
You can send us a DM on Telegram and we will respond ASAP
**t.me/worm_nft_dropper**


